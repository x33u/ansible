- change root passwd
- disable bash history over /etc/profile.d/
- set default .bashrc for new users
- disable ipv6 (see: hacking & security page 690)
- setup autologout in /etc/profile.d
-- see: netstat -tl
-- ip a l enp1s0
ssh ipv6 > AllowFamily inet
disable zonedrifting in firewalld (since its deprecated)
