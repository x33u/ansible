## == preconfigure system
- name: delete old hostkeys
  command: rm /etc/ssh/ssh_host_*

- name: generate /etc/ssh/ RSA host key
  command: ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ""
  args:
    creates: /etc/ssh/ssh_host_rsa_key

- name: generate /etc/ssh/ ed25519 host key
  command: ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ""
  args:
    creates: /etc/ssh/ssh_host_ed25519_key

- name: remove small diffie-hellman moduli
  ansible.builtin.shell:
    cmd: awk '$5 >= 3071' /etc/ssh/moduli > /etc/ssh/moduli.safe ; mv /etc/ssh/moduli.safe /etc/ssh/moduli

- name: restrict supported key exchange, cipher, and MAC algorithms
  ansible.builtin.shell:
    cmd: echo -e "\n# Restrict key exchange, cipher, and MAC algorithms, as per sshaudit.com\n# hardening guide.\nKexAlgorithms sntrup761x25519-sha512@openssh.com,curve25519-sha256,curve25519-sha256@libssh.org,gss-curve25519-sha256-,diffie-hellman-group16-sha512,gss-group16-sha512-,diffie-hellman-group18-sha512,diffie-hellman-group-exchange-sha256\nCiphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr\nMACs hmac-sha2-256-etm@openssh.com,hmac-sha2-512-etm@openssh.com,umac-128-etm@openssh.com\nHostKeyAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com" > /etc/ssh/sshd_config.d/ssh-audit_hardening.conf

## == ssh config
- name: update sshd_config
  ansible.builtin.template:
    src: "sshd_config"
    dest: /etc/ssh/sshd_config
  notify: sshd-restart

- name: add sshd-banner
  ansible.builtin.copy:
    src: "sshd-banner"
    dest: /etc/ssh/sshd-banner
  notify: sshd-restart

- name: restart service sshd, in all cases
  ansible.builtin.service:
    name: sshd
    state: restarted


## == fail2ban
- name: install fail2ban
  ansible.builtin.apt:
    name: fail2ban
    state: present
  notify: fail2ban-restart

- name: copy jail.local
  ansible.builtin.template:
    src: "jail.local"
    dest: "/etc/fail2ban/jail.local"
    owner: root
    group: root
    mode: '0644'
  notify: fail2ban-restart

- name: copy proxmox filter
  ansible.builtin.copy:
    src: "proxmox.conf"
    dest: "/etc/fail2ban/filter.d/proxmox.conf"
    owner: root
    group: root
    mode: '0644'
  notify: fail2ban-restart

## == configure root
- name: change root password
  user:
    name: root
    password: "{{ vault_root_image_password }}"
    update_password: always

- name: copy unset_history.sh to /etc/profile.d
  ansible.builtin.copy:
    src: "unset_history.sh"
    dest: /etc/profile.d/unset_history.sh
    owner: root
    group: root
    mode: '0644'

- name: copy autologout.sh to /etc/profile.d
  ansible.builtin.copy:
    src: "autologout.sh"
    dest: /etc/profile.d/autologout.sh
    owner: root
    group: root
    mode: '0644'

- name: copy bashrc to /etc/skel
  ansible.builtin.copy:
    src: "bashrc"
    dest: "/etc/skel/.bashrc"
    owner: root
    group: root
    mode: '0644'

## == create user
- name: install zsh
  ansible.builtin.apt:
    name: zsh
    state: present

- name: copy zshrc to root
  ansible.builtin.copy:
    src: "zshrc"
    dest: "/root/.zshrc"

- name: change root default shell to zsh
  user:
    name: "root"
    shell: /bin/zsh

- name: copy tmux.conf to root
  ansible.builtin.copy:
    src: "tmux.conf"
    dest: "/root/.tmux.conf"

- name: copy nanorc to root
  ansible.builtin.copy:
    src: "nanorc"
    dest: "/root/.nanorc"

- name: extract nanorc-file.tar into /root/.nano
  ansible.builtin.unarchive:
    src: nanorc.tar
    dest: "/root/"
    mode: '0644'

- name: copy zshrc to users
  ansible.builtin.copy:
    src: "zshrc"
    dest: "/etc/skel/.zshrc"

- name: copy tmux.conf to users
  ansible.builtin.copy:
    src: "tmux.conf"
    dest: "/etc/skel/.tmux.conf"

- name: copy nanorc to users
  ansible.builtin.copy:
    src: "nanorc"
    dest: "/etc/skel/.nanorc"

- name: extract nanorc-file.tar into /etc/skel/.nano
  ansible.builtin.unarchive:
    src: nanorc.tar
    dest: "/etc/skel/"

- name: add users
  ansible.builtin.user:
    name: "{{ item.name }}"
    comment: "{{ item.comment }}"
    shell: "{{ item.shell }}"
    groups: "{{ item.groups }}"
    password: "{{ item.password }}"
  with_items: "{{ [admin_details, user_details, ansible_details] }}"

- name: set public ssh keys for users
  ansible.posix.authorized_key:
    user: "{{ item.name }}"
    state: present
    key: "https://gitlab.com/{{ item.name }}.keys"
  with_items: "{{ [admin_details, user_details, ansible_details] }}"

- name: ensure users changed password at first time
  command: chage -E -1 -d 0 "{{ item.name }}"
  with_items: "{{ [admin_details, user_details] }}"

- name: copy sudoers config file to /etc/sudoers.d
  ansible.builtin.template:
    src: "clients"
    dest: "/etc/sudoers.d/clients"


