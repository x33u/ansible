set hostname
change sshd
add sshd-banner
restart sshd
change hosts
change default shell to zsh for root
configure zsh for users and root
configure nano for users and root
configure tmux for users and root

