install nodeexporter (epel)
tls setup
credential setup

openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 \
-keyout node-exporter.key \
-out node-exporter.crt \
-subj "/C=DE/ST=SA/L=Magdeburg/O=x33u.org/CN=node-exporter"

import getpass
import bcrypt

password = getpass.getpass("password: ")
hashed_password = bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt())
print(hashed_password.decode())
